Example package project from https://packaging.python.org/tutorials/packaging-projects/

1. Create package files
2. Create setup.py
3. Create readme and license
4. Generate distribution archives
    - `python3 -m pip install --user --upgrade setuptools wheel`
    - `python3 setup.py sdist bdist_wheel`
    - this generates `tar.gz` and `.whl`
5. Upload the distribution archives
    - `python3 -m pip install --user --upgrade twine`
    - `python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*`
6. Install your newly uploaded package
    - `python3 -m pip install --index-url https://test.pypi.org/simple/ --no-deps example-pkg-mkwon`