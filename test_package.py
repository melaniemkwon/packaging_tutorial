"""
https://test.pypi.org/manage/projects/

python3 -m pip install --index-url https://test.pypi.org/simple/ --no-deps example-pkg-mkwon
"""
import example_pkg

if __name__ == "__main__":
    print(example_pkg.name)
    print("hello new package")